<?php

namespace App\Http\Controllers\Demo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DemoController extends Controller
{
    public function Index(){
        return view('demo');
    } // end method
    public function DemoTwo(){
        return view('demotwo');
    } // end method
}
